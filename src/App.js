import React from "react";
import './App.css';
import Home from "./Components//home.jsx";
import Screen1 from "./Components/screen1.jsx";
import Screen2 from "./Components/screen2.jsx";
import Screen3 from "./Components/screen3.jsx";
import Screen4 from "./Components/screen4.jsx";
import Footer from "./Components/footer.jsx";

import Particles from "react-particles-js";

const particlesOptions = {
  particles: {
    number: {
      value: 50,
      density: {
        enable: true,
        value_area: 700,
      },
    },
    move:{
      speed:3
    }
  },
  interactivity: {
    detect_on: "canvas",
    events: {
      onhover: {
        enable: true,
        mode: "repulse",
      },
    },
  },
};

function App() {
  return (
    <div className="App">
      <Particles className="particles" params={particlesOptions} />
      <Home />
      <Screen1 />
      <Screen2 />
      <Screen3 />
      <Screen4 />
      <Footer />
    </div>
  );
}

export default App;
