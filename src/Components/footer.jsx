import "./style.css";
import React from "react";
import {
  Button,
  Grid,
  Header,
  Segment,
  Input,
  Menu,
  Icon,
} from "semantic-ui-react";
import PrivacyPolicy from "./privacyPolicy.jsx";

const Footer = ({ mobile }) => (
  <Segment style={{ padding: "1em 0em" }} vertical>
    <Grid
      className="grid"
      textAlign="center"
      container
      stackable
      verticalAlign="middle"
    >
      <Grid.Row width={5}>
        <Header
          inverted
          textAlign={"center"}
          style={{
            fontSize: "3em",
            fontWeight: "normal",
            fontStyle: "italic",
          }}
        >
          Never stop pursuing your curiosity <br></br>
          on your way to reach the stars
        </Header>
      </Grid.Row>

      <Grid.Row
        className="grid_row"
        style={{ minHeight: "50vh", paddingTop: 0 }}
      >
        <Grid.Column width={5}>
          <Header inverted style={{ fontWeight: "normal" }}>
            We are based in
          </Header>

          <Header inverted style={{ marginTop: "-0.5em" }}>
            Cluj-Napoca, Romania
          </Header>

          <Header inverted style={{ marginTop: "2em" }}>
            +0748400346
          </Header>

          <Header inverted style={{ marginTop: "-0.5em" }}>
            xcitedigitalcoffee@gmail.com
          </Header>
        </Grid.Column>

        <Grid.Column width={8}>
          <Input
            transparent
            inverted
            size="big"
            placeholder="Your email here"
          />
          <Button inverted circular>
            Keep me updated
          </Button>
        </Grid.Column>

        <Grid.Column width={3}>
          <Header inverted style={{ margin: "0 1em" }}>
            Stay in touch
          </Header>
          <Menu.Item
            href="https://www.facebook.com/xcitedigitalcoffee"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Icon name="facebook" size="huge" link />
          </Menu.Item>
          <Menu.Item
            href="https://www.linkedin.com/in/alexandra-ureche-250891184/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <Icon inverted name="linkedin" size="huge" link />
          </Menu.Item>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row
        style={{
          fontSize: "0.7em",
          position: "relative",
        }}
      >
        2020 © Xcite.All rights reserved. <PrivacyPolicy />
      </Grid.Row>
    </Grid>
  </Segment>
);

export default Footer;
