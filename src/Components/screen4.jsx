import "./style.css";
import React from "react";

import { Grid, Header, Segment, Divider } from "semantic-ui-react";

const Screen4 = ({ mobile }) => (
  <Segment style={{ padding: "1em 0em" }} vertical>
    <Grid className="grid" container stackable verticalAlign="middle">
      <Grid.Row className="grid_row" style={{ minHeight: "100vh" }}>
        <Grid.Column width={13}>
          <Header
            inverted
            style={{
              fontSize: "3em",
            }}
          >
            The
          </Header>
          <Header
            inverted
            style={{
              fontSize: "5em",
              margin: '-0.7em 0'
            }}
          >
            Journey and Road 
          </Header>
          <Header
            inverted
            textAlign={'center'}
            style={{
              fontSize: "3em"
            }}
          >
            ahead
          </Header>
          <Header
            inverted
            style={{
              fontWeight: "normal",
              fontSize: "1.5em",
            }}
          >
            Having the honour to start as winners of the Innovation Labs
            Hackathon in Cluj, we pursued our passion in designing and
            reimagining the device and the potential and advantages of this
            technology.
          </Header>
          <Header
            inverted
            style={{
              fontWeight: "normal",
              fontSize: "1.5em",
            }}
          >
            Right now, our interdisciplinary team is working on creating the
            most ergonomic and efficient MVP, hoping to make this personal
            cognitive enhancing tool tailored by people’s needs, A ReaLLiTY
          </Header>
        </Grid.Column>
      </Grid.Row>
      <Divider inverted />
    </Grid>
  </Segment>
);

export default Screen4;
