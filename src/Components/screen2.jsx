import "./style.css";
import React from "react";

import { Grid, Header, Image, Segment } from "semantic-ui-react";

import Brain from "../Assets/brain2.PNG";

const Screen2 = ({ mobile }) => (
  <Segment style={{ padding: "1em 0em" }} vertical>
    <Grid className="grid" container stackable verticalAlign="middle">
      <Grid.Row className="grid_row" style={{ minHeight: "100vh" }}>
        <Grid.Column width={6}>
          <Image size="massive" src={Brain} />
        </Grid.Column>

        <Grid.Column width={10}>
          <Header
            inverted
            style={{
              fontSize: "5em",
            }}
          >
            {" "}
            Nerdy explanation{" "}
          </Header>
          <Header
            inverted
            style={{
              fontSize: "3em",
              marginTop: "-0.3em",
            }}
          >
            {" "}
            for what just{" "}
          </Header>
          <Header
            inverted
            textAlign={"center"}
            style={{
              fontSize: "5em",
              marginTop: "-0.5em",
            }}
          >
            {" "}
            happened{" "}
          </Header>

          <Header
            inverted
            style={{
              fontSize: "1.5em",
              fontWeight: "normal",
            }}
          >
            Studies show us, that when scientists go craZZy and choose to create
            small impulses in different brain areas, that leads to enhanced{" "}
            <a
              href="https://www.psychologytoday.com/blog/beautiful-minds/201204/brain-stimulation-makes-the-impossible-problem-solvable"
              target="_blank"
              rel="noopener noreferrer"
            >
              heighten creativity,{" "}
            </a>
            <a
              href="http://www.ncbi.nlm.nih.gov/pubmed/24992897"
              target="_blank"
              rel="noopener noreferrer"
            >
              spatial learning
            </a>
            , boost{" "}
            <a
              href="http://www.cell.com/current-biology/abstract/S0960-9822(13)00486-7"
              target="_blank"
              rel="noopener noreferrer"
            >
              math skills
            </a>{" "}
            and{" "}
            <a
              href="http://www.ncbi.nlm.nih.gov/pubmed/18303984"
              target="_blank"
              rel="noopener noreferrer"
            >
              language acquisition
            </a>{" "}
            and even trigger{" "}
            <a
              href="http://www.nature.com/neuro/journal/v17/n6/abs/nn.3719.html"
              target="_blank"
              rel="noopener noreferrer"
            >
              lucid dreams
            </a>{" "}
            — sometimes{" "}
            <a
              href="http://www.cell.com/current-biology/abstract/S0960-9822(13)00486-7"
              target="_blank"
              rel="noopener noreferrer"
            >
              weeks after the initial stimulation.
            </a>
          </Header>
          <Header
            inverted
            style={{
              fontSize: "1.5em",
              fontWeight: "normal",
            }}
          >
            This technology also proved to improve recognition memory in{" "}
            <a
              href="https://n.neurology.org/content/71/7/493.short"
              target="_blank"
              rel="noopener noreferrer"
            >
              Alzheimer disease
            </a>{" "}
            with scientists trying to find applications for other psychiatric
            disease such as{" "}
            <a
              href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5977072/"
              target="_blank"
              rel="noopener noreferrer"
            >
              depression.
            </a>
          </Header>
          <Header
            inverted
            style={{
              fontSize: "1.5em",
              fontWeight: "normal",
            }}
          >
            PS:{" "}
            <a
              href="https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6499187/"
              target="_blank"
              rel="noopener noreferrer"
            >
              American soldiers
            </a>{" "}
            seem to enjoy it too
          </Header>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </Segment>
);

export default Screen2;
