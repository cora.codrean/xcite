import React from "react";
import { Grid, Header, Segment } from "semantic-ui-react";
import "./style.css";

const Screen1 = ({ mobile }) => (
  <Segment style={{ padding: "1em 0em" }} vertical>
    <Grid className="grid" container stackable verticalAlign="middle" centered>
      <Grid.Row className="grid_row" style={{ minHeight: "100vh" }}>
        <Grid.Column width={8}>
          <Header
            inverted
            size={"huge"}
            textAlign={'center'}  
            style={{
              fontFamily: "Gilroy",
              fontWeight: "normal",
            }}
          >
            Xcite is a neurostimulation device shaped as a headband, designed to
            improve one’s abilities, such as memory and problem solving, while
            promoting creativity and improved motor skills.
          </Header>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </Segment>
);

export default Screen1;
