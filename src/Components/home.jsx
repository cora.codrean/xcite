import React from "react";
import PropTypes from "prop-types";
import {
  Button,
  Container,
  Grid,
  Header,
  Image,
  Input,
} from "semantic-ui-react";
import "./style.css";
import LogoXcite from "../Assets/xcite-logo-wb.png";
import PrivacyPolicy from "./privacyPolicy.jsx";

const Home = ({ mobile }) => (
  <Container
    width={8}
    textAlign="center"
    className="grid_row home"
    style={{ minHeight: "100vh" }}
  >
    <Image src={LogoXcite} size="small" centered></Image>
    <Grid className="grid" container stackable verticalAlign="middle">
      <Grid.Row centered textAlign={"center"}>
        <Header
          inverted
          style={{
            fontSize: "7em",
            fontWeight: "normal",
            position: "relative",
          }}
        >
          Get xcited!
        </Header>
      </Grid.Row>
    </Grid>

    <Header
      inverted
      size="huge"
      style={{
        fontWeight: "normal",
        position: "relative",
      }}
    >
      Do you want to become the real Flash?
    </Header>
    <Header
      inverted
      size="huge"
      style={{
        fontWeight: "normal",
        margin: "-0.5em 0 3em 0",
        position: "relative",
      }}
    >
      We know you do!
    </Header>
    <Input 
      transparent inverted size="massive" 
      placeholder="Your email here" 
      style={{marginRight:'2em'}} />
    <Button
      inverted
      basic
      color="teal"
      circular
      size="huge"
      style={{position: "relative",
              marginLeft:'2em' }}
    >
      {" "}
      Keep me updated
    </Button>
    <Grid.Row
      style={{
        fontSize: "0.7em",
        fontWeight: "normal",
        position: "relative",
        padding:'2em'
      }}
    >
      By submiting you agree to the <PrivacyPolicy />
    </Grid.Row>
  </Container>
);

Home.propTypes = {
  mobile: PropTypes.bool,
};

export default Home;
