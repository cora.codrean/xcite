import React from "react";
import { Grid, Header, Segment } from "semantic-ui-react";
import "./style.css";

const Screen3 = ({ mobile }) => (
  <Segment style={{ padding: "1em 0em" }} vertical>
    <Grid className="grid" container stackable verticalAlign="middle" centered>
      <Grid.Row className="grid_row" style={{ minHeight: "100vh" }}>
        <Grid.Column width={9}>
          <Header
            inverted
            size={"huge"}
            textAlign={"center"}
            style={{
              fontFamily: "Gilroy",
              fontWeight: "normal",
            }}
          >
            We envisioned Xcite as a personal cognitive enhancing tool, tailored
            by peoples needs.
          </Header>
          <Header
            inverted
            size={"huge"}
            textAlign={"center"}
            style={{
              fontFamily: "Gilroy",
              fontWeight: "normal",
            }}
          >
            Each individual device is 3D printed and
            customised to fit your demands, by taking into consideration head
            measurements and desired cognitive effect.
          </Header>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  </Segment>
);

export default Screen3;
